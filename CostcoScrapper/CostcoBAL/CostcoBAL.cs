﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CostcoScrapper.CostcoDAL;
using MySql.Data.MySqlClient;
using System.Data;

namespace CostcoScrapper.CostcoBAL
{
    public class CostcoBAL
    {
        string conCostco = string.Empty;
        public CostcoBAL(string _conCostco)
        {
            conCostco = _conCostco;
        }

        public int InsertBulkData(string path, string tableName)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(conCostco))
                {
                    con.Open();
                    MySqlBulkLoader bLoad = new MySqlBulkLoader(con);

                    bLoad.TableName = tableName;
                    bLoad.FieldTerminator = "|";
                    bLoad.LineTerminator = "\n";
                    bLoad.FileName = path;
                    bLoad.NumberOfLinesToSkip = 0;

                    bLoad.Load();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                //ErrorLog(ex, "", "InsertBulkData", "", "");
                throw;

            }
            return 0;
        }

        public DataTable ReturnDataTable(string query)
        {
            try
            {
                DataTable dt = new DataTable();
                using (MySqlConnection con = new MySqlConnection(conCostco))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    MySqlCommand cmd = new MySqlCommand(query);
                    cmd.Connection = con;
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public DataTable GetCostcoSnacksLink(string tableName)
        {
            DataTable dt = new DataTable();
            try
            {
                string query = string.Format("SELECT * FROM {0} WHERE isProcessed=1;", tableName);
                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
            return dt;
        }


        public void InsertCostcoData(CostcoData objData)
        {
            string query = string.Empty;
            try
            {
                using (MySqlConnection con = new MySqlConnection(conCostco))
                {
                    con.Open();
                    if(objData.PromotionText.Contains("'"))
                    {
                        objData.PromotionText = objData.PromotionText.Replace("'", "''");
                    }
                    if (objData.Description.Contains("'"))
                    {
                        objData.Description = objData.Description.Replace("'", "''");
                    }
                    if (objData.Features.Contains("'"))
                    {
                        objData.Features = objData.Features.Replace("'", "''");
                    }
                    if (objData.Title.Contains("'"))
                    {
                        objData.Title = objData.Title.Replace("'", "''");
                    }
                    query = string.Format("INSERT INTO data_costco (Title,ItemNumber,OnlinePrice,YourPrice,LessPrice,DeliveryFee,PriceBox,PromotionText,Description,Features,ImageUrl1,ImageUrl2,ImageUrl3,ImageUrl4,ImageUrl5,LinkID,CreatedOn) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',{15},'{16}'); UPDATE link_costco SET isProcessed=0 WHERE ID={15};", objData.Title, objData.ItemNumber, objData.OnlinePrice, objData.YourPrice, objData.LessPrice, objData.DeliveryFee, objData.PricePerBOX, objData.PromotionText, objData.Description, objData.Features,objData.ImageUrl1,objData.ImageUrl2,objData.ImageUrl3,objData.ImageUrl4,objData.ImageUrl5, objData.LinkID,DateTime.Now.ToString());
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ErrorLog(Exception ex, string Url, string Functionname, string PartName, string PartNumber)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(conCostco))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    string query = string.Format("INSERT INTO ErrorLog (URL,FunctionName,ExceptionMessage,StackTrace,PartName,PartNumber,CreatedOn) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',UTC_TIMESTAMP());", Url, Functionname, ex.Message, ex.StackTrace, PartName, PartNumber);
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception exp)
            {
                throw;
            }
        }
    }
}
