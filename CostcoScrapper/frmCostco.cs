﻿using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CostcoScrapper.CostcoDAL;
using CostcoScrapper.CostcoBAL;

namespace CostcoScrapper
{
    public partial class frmCostco : Form
    {
        ScrapHelper.ScrapHelper _scrapHelper = new ScrapHelper.ScrapHelper(Convert.ToInt32(ConfigurationSettings.AppSettings["_retry"].Trim()));
        CostcoBAL.CostcoBAL _objCostcoBal = new CostcoBAL.CostcoBAL(ConfigurationSettings.AppSettings["CostcoDb"].ToString());
        string Url = "https://www.costco.com";
        public frmCostco()
        {
            InitializeComponent();
        }

        private void frmCostco_Shown(object sender, EventArgs e)
        {
            Thread.Sleep(100);
            ScrapLinkDetails();
            
        }
        public int GetPages(int products)
        {
            int pages = 0;
            try
            {
                if(products>0)
                {
                    pages = Convert.ToInt32(Math.Round((Convert.ToDecimal(products) / 96),2))+1;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return pages;
        }

        public void ScrapLinkDetails()
        {
            try
            {
                lblCostcoStart.ForeColor = Color.Green;
                lblCostcoStart.Text = "Snacks Link scrapning start : " + DateTime.Now;
                lblCostcoStart.Refresh();

                string Url = "https://www.costco.com/snacks.html";
                string totalProducts = string.Empty;
                int products = 0;
                int pages = 0;
                string content = string.Empty;
                StringBuilder sbLinks = new StringBuilder();
                string CSVPath = string.Empty, FileName = string.Empty;
                content = _scrapHelper.GetSource(Url);
                totalProducts = HtmlHelper.ReturnValue(content, "<div class=\"table-cell results hidden-xs hidden-sm hidden-md\">", "<span>", "</span>");
                if (totalProducts.Contains("of"))
                {
                    products = HtmlHelper.ReturnValue(content, "<div class=\"table-cell results hidden-xs hidden-sm hidden-md\">", "of", "</span>") != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(content, "<div class=\"table-cell results hidden-xs hidden-sm hidden-md\">", "of", "</span>")) : 0;
                }
                if (products > 0)
                {
                    pages = GetPages(products);
                }
                for (int i = 1; i <= pages; i++)
                {
                    if (i == 1)
                    {
                        Url = "https://www.costco.com/snacks.html";
                    }
                    else
                    {
                        Url = string.Format("https://www.costco.com/snacks.html?currentPage={0}&pageSize=96", i);
                    }
                    content = _scrapHelper.GetSource(Url);
                    string[] arrCategory = content.Split(new string[] { "data-pdp-url" }, StringSplitOptions.None);
                    if (arrCategory.Length > 1)
                    {
                        for (int j = 1; j < arrCategory.Length; j++)
                        {
                            string LinkUrl = HtmlHelper.ReturnValue(arrCategory[j], "=\"", "\" item-index");
                            sbLinks.AppendLine("0|" + LinkUrl + "|1|" + DateTime.Now.ToString() + "|");
                        }
                    }
                }
                CSVPath = ConfigurationSettings.AppSettings["CSVCostco"].ToString() + "snacks";
                if (!System.IO.Directory.Exists(CSVPath))
                {
                    System.IO.Directory.CreateDirectory(CSVPath);
                }
                FileName = "snacks" + "_" + DateTime.Now.ToString("ddMMyyy") + "_" + Guid.NewGuid().ToString() + ".csv";
                System.IO.File.WriteAllText(CSVPath + "\\" + FileName, sbLinks.ToString());
                int result = _objCostcoBal.InsertBulkData(CSVPath + "\\" + FileName, "link_costco");

                if (result == 1)
                {
                    DataTable dtLink = _objCostcoBal.GetCostcoSnacksLink("link_costco");
                    ScrapData(dtLink);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }
        public void ScrapData(DataTable dtLink)
        {
            try
            {
                
                if (dtLink.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtLink.Rows)
                    {
                        string linkUrl = dr["URL"] != null ? dr["URL"].ToString() : "";//"https://www.costco.com/honey-maid-graham-crackers-14.4-oz%2c-4-count.product.100369056.html"; //"https://www.costco.com/harvest-snaps-organic-green-pea-snack-crisps%2c-lightly-salted%2c-20-oz.product.100462162.html"; //"https://www.costco.com/nature-valley-fruit-%2526-nut-chewy-granola-bar%2c-trail-mix%2c-1.2-oz%2c-48-count.product.100381630.html"; //"https://www.costco.com/honey-maid-graham-crackers-14.4-oz%2c-4-count.product.100369056.html";
                        int linkID = dr["ID"] != null ? Convert.ToInt32(dr["ID"].ToString()) : 0;
                        string content = _scrapHelper.GetSource(linkUrl, "");
                        if (content != "")
                        {
                            string title = HtmlHelper.ReturnValue(content, "<h1 itemprop=\"name\">", "</h1>") != "" ? HtmlHelper.ReturnValue(content, "<h1 itemprop=\"name\">", "</h1>") : "";
                            string itemNumber = HtmlHelper.ReturnValue(content, "<span data-sku=\"", "itemprop=\"sku\">", "</span>") != "" ? HtmlHelper.ReturnValue(content, "<span data-sku=\"", "itemprop=\"sku\">", "</span>") : "";
                            string deliveryFee = HtmlHelper.ReturnValue(content, "<div id=\"grocery-fee-amount\"", ">", "</div>") != "" ? HtmlHelper.ReturnValue(content, "<div id=\"grocery-fee-amount\"", ">", "</div>") : "";
                            string pricePerBOX = HtmlHelper.ReturnValue(content, "<span class=\"unitcurrency\">", "</span>") != "" ? HtmlHelper.ReturnValue(content, "<span class=\"unitcurrency\">", "</span>") : "";
                            string promotionText = HtmlHelper.ReturnValue(content, "<p class=\"PromotionalText\">", "<a");
                            string o_description = HtmlHelper.ReturnValue(content, "<div class=\"grocery-business-delivery form-group body-copy-medium success grocery-espot-class\">", "</div>");
                            string removeCommentDes = Regex.Replace(o_description, "<!--.*?-->", String.Empty, RegexOptions.Multiline);
                            string description = StripTagsRegex(removeCommentDes);
                            string ul_features = HtmlHelper.ReturnValue(content, "<ul class=\"pdp-features\">", "</ul>");
                            string features = ul_features != "" ? ul_features.Replace("<li>", "").Replace("</li>", "|").TrimEnd(',') : "";


                            #region
                            string itemId = HtmlHelper.ReturnValue(content, "<input type=\"hidden\" class=\"catentryInput\" id=\"catEntryId\" name=\"catEntryId\" value=\"", "\"/>");
                            string catalogId = HtmlHelper.ReturnValue(content, "<input type=\"hidden\" name=\"catalogId\" value=\"", "\"/>");
                            string productId = HtmlHelper.ReturnValue(content, "<input type=\"hidden\" name=\"productBeanId\" value=\"", "\"/>");

                            string price_Url = string.Format("https://www.costco.com/AjaxGetContractPrice?itemId={0}&catalogId={1}&productId={2}&WH=729-bd", itemId, catalogId, productId);
                            string priceContent = _scrapHelper.GetJSONSource(linkUrl, price_Url);

                            string onlinePrice = string.Empty;
                            string o_price = string.Empty;
                            string o_currency = string.Empty;
                            o_price = HtmlHelper.ReturnValue(priceContent, "{\"finalOnlinePrice\":", ",") != "" ? HtmlHelper.ReturnValue(priceContent, "{\"finalOnlinePrice\":", ",") : "";
                            o_currency = HtmlHelper.ReturnValue(content, "<span class=\"currency\">", "</span>") != "" ? HtmlHelper.ReturnValue(content, "<span class=\"currency\">", "</span>") : "";
                            if (o_price != "") { 
                            onlinePrice = string.Format("{0}{1}", o_currency, o_price);
                            }
                            else
                            {
                                onlinePrice = "";
                            }
                            string lessPrice = string.Empty;
                            string l_price = string.Empty;
                            string l_currency = string.Empty;
                            string isMinus = string.Empty;
                            l_price = HtmlHelper.ReturnValue(priceContent, "\"discount\":", ",") != "" ? HtmlHelper.ReturnValue(priceContent, "\"discount\":", ",") : "";
                            isMinus = HtmlHelper.ReturnValue(content, "<span class=\"minus\">", "", "</span>") != "" ? HtmlHelper.ReturnValue(content, "<span class=\"minus\">", "</span>") : "";
                            if (isMinus != "")
                            {
                                l_currency = HtmlHelper.ReturnValue(content, "<span class=\"currency\">", "<span ", "</span>") != "" ? HtmlHelper.ReturnValue(content, "<span class=\"currency\">", "</span>") : "";
                            }
                            if (l_price != "")
                            {
                                lessPrice = l_price != "" ? string.Format("{0}{1}{2}", isMinus, l_currency, l_price) : "";
                            }
                            else
                            {
                                lessPrice = "";
                            }
                            string yourPrice = string.Empty;
                            if (o_price != "" && l_price != "")
                            {
                                string y_Price = o_price != "" && l_price != "" ? (Convert.ToDecimal(o_price) - Convert.ToDecimal(l_price)).ToString() : "";
                                yourPrice = y_Price != "" ? string.Format("{0}{1}", l_currency, y_Price) : "";
                            }
                            else
                            {
                                yourPrice = HtmlHelper.ReturnValue(content, "<meta property=\"product:price:amount\" content=\"", "\"/>");
                                yourPrice = string.Format("${0}", yourPrice);
                            }
                            #endregion

                            #region Get images
                            string profileid = string.Empty;
                            string itemid = string.Empty;
                            string viewerid = "1068";
                            string callback = string.Empty;
                            profileid = HtmlHelper.ReturnValue(content, "profileId=", "&");
                            itemid = HtmlHelper.ReturnValue(content, "itemId=", "&");//itemId=204529-847&
                            callback = string.Format("productXmlCallbackImageColorChangeprofileid{0}itemid{1}", profileid, itemid.Contains("-") ? itemid.Replace("-", string.Empty) : "");//callback=productXmlCallbackImageColorChangeprofileid12026540itemid204529847

                            //https://richmedia.ca-richimage.com/ViewerDelivery/productXmlService?profileid=12026540&itemid=204529-847&viewerid=1068&callback=productXmlCallbackImageColorChangeprofileid12026540itemid204529847

                            string imageUrl = string.Format("https://richmedia.ca-richimage.com/ViewerDelivery/productXmlService?profileid={0}&itemid={1}&viewerid={2}&callback={3}", profileid, itemid, viewerid, callback);
                            string imageContent = _scrapHelper.GetSource(imageUrl);

                            string imageUrl1 = string.Empty, imageUrl2 = string.Empty, imageUrl3 = string.Empty, imageUrl4 = string.Empty, imageUrl5 = string.Empty;
                            string[] arr = HtmlHelper.CollectUrl(imageContent, "\"@type\":\"source\"", "images", ",", "text");
                            if(arr.Length>0)
                            {
                                for(int k=0;k<arr.Length;k++)
                                {
                                  if(arr[k].Contains("@path"))
                                    {
                                        if (imageUrl1 == string.Empty)
                                        { 
                                        imageUrl1 = HtmlHelper.ReturnValue(arr[k], "@path\":\"", "\",");
                                            continue;
                                        }
                                        if (imageUrl2 == string.Empty)
                                        {
                                            imageUrl2= HtmlHelper.ReturnValue(arr[k], "@path\":\"", "\",");
                                            continue;
                                        }
                                        if (imageUrl3 == string.Empty)
                                        {
                                            imageUrl3 = HtmlHelper.ReturnValue(arr[k], "@path\":\"", "\",");
                                            continue;
                                        }
                                        if (imageUrl4 == string.Empty)
                                        {
                                            imageUrl4 = HtmlHelper.ReturnValue(arr[k], "@path\":\"", "\",");
                                            continue;
                                        }
                                        if (imageUrl5 == string.Empty)
                                        {
                                            imageUrl5 = HtmlHelper.ReturnValue(arr[k], "@path\":\"", "\",");
                                            continue;
                                        }
                                    }
                                }
                            }
                            #endregion
                            CostcoData objData = new CostcoDAL.CostcoData()
                            {
                                Title = title,
                                ItemNumber = itemNumber,
                                OnlinePrice = onlinePrice,
                                LessPrice = lessPrice,
                                YourPrice = yourPrice,
                                DeliveryFee = deliveryFee,
                                PricePerBOX = pricePerBOX,
                                PromotionText = promotionText,
                                Description = description,
                                Features = features,
                                ImageUrl1=imageUrl1,
                                ImageUrl2 = imageUrl2,
                                ImageUrl3 = imageUrl3,
                                ImageUrl4 = imageUrl4,
                                ImageUrl5 = imageUrl5,
                                LinkID=linkID
                            };
                            _objCostcoBal.InsertCostcoData(objData);
                        }
                    }
                }
                lblCostcoEnd.ForeColor = Color.Green;
                lblCostcoEnd.Text = "Snacks Link scrapning end : " + DateTime.Now;
                lblCostcoEnd.Refresh();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}


