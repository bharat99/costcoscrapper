﻿using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CostcoScrapper.ScrapHelper
{
    public class ScrapHelper
    {
        int retryCount = 0;
        int ProxyChange = 0;
        public ScrapHelper(int _retryCount)
        {
            retryCount = _retryCount;
        }
        public string GetSource(string url, string scookie = "")
        {
            string Content = String.Empty;
            int retry = 0;
            int defaultRetry = 0;
            string myPageSource = string.Empty;
        retryContent:
            try
            {
                defaultRetryContent:
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                ServicePointManager.Expect100Continue = false;
                ServicePointManager.MaxServicePointIdleTime = 0;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;

                //if (Convert.ToBoolean(ConfigurationSettings.AppSettings["_proxy"].Trim()))
                //{
                //    myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                //    if (ProxyChange >= 16)
                //        ProxyChange = 0;
                //}

                //myWebRequest.Referer =  url;
                myWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36";
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9,sv;q=0.8";
                myWebRequest.Headers["accept-encoding"] = "gzip, deflate, br";
                myWebRequest.Headers["authority"] = "www.costco.com";//"costco.tt.omtrdc.net";
                myWebRequest.Headers["sec-fetch-mode"] = "navigate";
                myWebRequest.Headers["sec-fetch-dest"] = "document";
                myWebRequest.Headers["sec-fetch-site"] = "none";
                myWebRequest.Headers["sec-fetch-user"] = "?1";
                myWebRequest.Headers["cookie"] = "invCheckPostalCode=10001; invCheckStateCode=NY;";
               myWebRequest.Headers["Upgrade-Insecure-Requests"] = "1";
                //myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";
                

                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;

                using (var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse())
                {
                    Stream responseStream = myWebResponse.GetResponseStream();
                    StreamReader myWebSource = new StreamReader(responseStream, Encoding.Default);
                    myPageSource = myWebSource.ReadToEnd();
                }

                if(defaultRetry<5)
                {
                    defaultRetry++;
                    goto defaultRetryContent;
                }
                if (myPageSource == "" && retry < retryCount)
                {
                    retry++;
                    Thread.Sleep(1000);
                    goto retryContent;
                }

                return HtmlHelper.ReturnFormatedHtml(myPageSource);
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("The server committed a protocol violation. Section=ResponseStatusLine".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("Received an unexpected EOF or 0 bytes from the transport stream.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The operation has timed out".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The remote server returned an error: (408) Request Timeout.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The remote server returned an error: (500) Internal Server Error.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("Execution Timeout Expired.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                    else
                    {
                        //CommonUtility.ErrorLog(ex, "Url : " + url, "GetSource");
                    }
                }
            }
            return myPageSource;
        }
        

        public string GetJSONSource(string url,string priceUrl, string scookie = "")
        {
            string Content = String.Empty;
            int retry = 0;
            string myPageSource = string.Empty;
        retryContent:
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                ServicePointManager.Expect100Continue = false;
                ServicePointManager.MaxServicePointIdleTime = 0;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(priceUrl);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;

                //if (Convert.ToBoolean(ConfigurationSettings.AppSettings["_proxy"].Trim()))
                //{
                //    myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                //    if (ProxyChange >= 16)
                //        ProxyChange = 0;
                //}

                myWebRequest.Referer = url;
                myWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36";
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9,sv;q=0.8";
                myWebRequest.Headers["accept-encoding"] = "gzip, deflate, br";
                myWebRequest.Headers["authority"] = "www.costco.com";//"costco.tt.omtrdc.net";

                //myWebRequest.Host = "www.costco.com";
                myWebRequest.Headers["sec-fetch-mode"] = "cors";
                myWebRequest.Headers["sec-fetch-dest"] = "empty";
                myWebRequest.Headers["sec-fetch-site"] = "same-origin";
                myWebRequest.Headers["cookie"] = "invCheckPostalCode=10001; invCheckStateCode=NY;";
                //  myWebRequest.Headers["Upgrade-Insecure-Requests"] = "1";
                myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";
                //if (retry == 0)
                //{
                //    myWebRequest.Headers["x-csrf-token"] = "m1wsU+gEQTQm6vHieKnoWFe0SItp2zSgURTiGO7zl1Q=";
                //}

                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;

                using (var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse())
                {
                    Stream responseStream = myWebResponse.GetResponseStream();
                    StreamReader myWebSource = new StreamReader(responseStream, Encoding.Default);
                    myPageSource = myWebSource.ReadToEnd();
                }

                if (myPageSource == "" && retry < retryCount)
                {
                    retry++;
                    Thread.Sleep(1000);
                    goto retryContent;
                }

                return HtmlHelper.ReturnFormatedHtml(myPageSource);
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("The server committed a protocol violation. Section=ResponseStatusLine".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("Received an unexpected EOF or 0 bytes from the transport stream.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The operation has timed out".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The remote server returned an error: (408) Request Timeout.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The remote server returned an error: (500) Internal Server Error.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("Execution Timeout Expired.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                    else
                    {
                        //CommonUtility.ErrorLog(ex, "Url : " + url, "GetSource");
                    }
                }
            }
            return myPageSource;
        }

        public static WebProxy SetProxySevere(int ChangeCount)
        {
            WebProxy setproxy = new WebProxy("world.proxymesh.com", 31280);
            if (ChangeCount <= 4)
            {
                setproxy = new WebProxy("us.proxymesh.com", 31280);
            }
            else
            {
                if (ChangeCount <= 8)
                {
                    setproxy = new WebProxy("us-il.proxymesh.com", 31280);
                }
                else
                {
                    if (ChangeCount <= 12)
                    {
                        setproxy = new WebProxy("us-fl.proxymesh.com", 31280);
                    }
                    else
                    {
                        if (ChangeCount <= 16)
                        {
                            setproxy = new WebProxy("us-ca.proxymesh.com", 31280);
                        }
                        else
                        {
                            setproxy = new WebProxy("us-dc.proxymesh.com", 31280);
                            ChangeCount = 0;
                        }
                    }
                }

            }
            return setproxy;
        }

        private static bool AllwaysGoodCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;

        }
    }
}
