﻿namespace CostcoScrapper
{
    partial class frmCostco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCostcoStart = new System.Windows.Forms.Label();
            this.lblCostcoEnd = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCostcoStart
            // 
            this.lblCostcoStart.AutoSize = true;
            this.lblCostcoStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCostcoStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCostcoStart.Location = new System.Drawing.Point(25, 55);
            this.lblCostcoStart.Name = "lblCostcoStart";
            this.lblCostcoStart.Size = new System.Drawing.Size(102, 18);
            this.lblCostcoStart.TabIndex = 0;
            this.lblCostcoStart.Text = "lblCostcoStart";
            // 
            // lblCostcoEnd
            // 
            this.lblCostcoEnd.AutoSize = true;
            this.lblCostcoEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCostcoEnd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCostcoEnd.Location = new System.Drawing.Point(25, 89);
            this.lblCostcoEnd.Name = "lblCostcoEnd";
            this.lblCostcoEnd.Size = new System.Drawing.Size(97, 18);
            this.lblCostcoEnd.TabIndex = 1;
            this.lblCostcoEnd.Text = "lblCostcoEnd";
            // 
            // frmCostco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 129);
            this.Controls.Add(this.lblCostcoEnd);
            this.Controls.Add(this.lblCostcoStart);
            this.Name = "frmCostco";
            this.Text = "Costco";
            this.Shown += new System.EventHandler(this.frmCostco_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCostcoStart;
        private System.Windows.Forms.Label lblCostcoEnd;
    }
}

