﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostcoScrapper.CostcoDAL
{
    public class CostcoData
    {
        public string Title { get; set; }
        public string DeliveryFee { get; set; }
        public string ItemNumber { get; set; }
        public string OnlinePrice { get; set; }
        public string YourPrice { get; set; }
        public string LessPrice { get; set; }
        public string PricePerBOX { get; set; }
        public string PromotionText { get; set; }

        public string Description { get; set; }
        public string Features { get; set; }

        public string ImageUrl1 { get; set; }
        public string ImageUrl2 { get; set; }
        public string ImageUrl3 { get; set; }
        public string ImageUrl4 { get; set; }
        public string ImageUrl5 { get; set; }
        public int LinkID { get; set; }

    }
}
